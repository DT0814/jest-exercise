import { pay, Bank } from './payment-service';

test('请测试 - 对于 pay 方法, 如果 bank 返回 true 那么它应当返回 SUCCESS', () => {
  const bank = new Bank();
  bank.doPaymentTransaction = jest.fn(() => true);

  expect(pay(10000, 'ACC-001', bank)).toEqual('SUCCESS');
});
